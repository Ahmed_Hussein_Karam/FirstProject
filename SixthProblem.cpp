#include <iostream>
#include <vector>

using namespace std;

void RecPermute(string,string, vector<string>&);
void ListPermutations(string);

int main()
{
    string s;
    cout<<"Type a word: ";
    cin>>s;
    ListPermutations(s);
    return 0;
}

void RecPermute(string soFar, string rest, vector<string>& store)
{
    /*the main idea is to put all probabilities for the 1st letter and, for each of those, we put all
      probabilities for 2nd letter.. and so on.*/
    if (rest == "")       // No more characters
    {
        bool found = false;
        for(int j=0; j < store.size(); j++)
            if(store[j]==soFar)
        {
            found = true;
            break;
        }
        if(found == false)
        {
            store.push_back(soFar);
            cout << soFar << endl;  // Print the word
        }
    }
    else           // Still more chars
    // For each remaining char
    for (int i = 0; i < rest.length(); i++) {
    string next = soFar + rest[i]; // Glue next char
    string remaining = rest.substr(0, i)+ rest.substr(i+1);
    RecPermute(next, remaining, store);
    }
}
// "wrapper" function
void ListPermutations(string s)
{
    vector<string> store; //to store already printed permutations.
    RecPermute("", s, store);
}
