#include <iostream>

using namespace std;
long long multGood(long m, long n)
{
    if(n==1)
        return m;
    else
    {
        if(n%2==0)
            return multGood(m,n/2)+multGood(m,n/2);
        else
            return multGood(m,n/2)+multGood(m,n/2)+m;
    }
}
int main()
{
    long m, n;
    cout << "Please enter m and n to calculate m * n: ";
    cin >> m >> n;
    cout << "\nm * n = " << multGood (m, n)<<endl;
    return 0;
}
