#include <iostream>

using namespace std;

int number = 0;

int convertToInt(string numberStr)
{
    int i = 0;

    if (numberStr[i] != '\0')
    {
        if (numberStr[i]<'0' || numberStr[i]>'9')
            throw "This is an invalid integer, cannot be converted.";

        else
        {
            number *= 10;
            number += numberStr[i] - '0';
            convertToInt(&numberStr[i+1]);
        }
    }
    return number;
}

int main()
{
    string s;
    cout<<"Enter an integer: ";
    cin>>s;
    try
    {
        cout<<"The converted number is "<<convertToInt(s)<<endl;
    }
    catch (const char* msg)
    {
        cout<<msg<<endl;
    }

    return 0;
}
